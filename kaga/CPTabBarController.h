//
//  CPTabBarController.h
//  akagi
//
//  Created by 王澍宇 on 15/11/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewController.h"

@interface CPTabBarController : CPBaseViewController

@property (nonatomic, strong) UIView *tabBar;

@property (nonatomic, strong) NSArray<CPNavigationController *> *viewControllers;

@property (nonatomic, strong) NSArray *images;

@property (nonatomic, strong) NSArray *selectedImages;

@property (nonatomic, strong) NSArray *titles;

@property (nonatomic, strong) CPNavigationController *currentController;

- (instancetype)initWithViewControllers:(NSArray *)viewControllers Titles:(NSArray *)titles Images:(NSArray *)images SelectedImages:(NSArray *)selectedImages;

@end
