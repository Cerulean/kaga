//
//  CPFileViewController.h
//  kaga
//
//  Created by 王澍宇 on 15/12/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseCollectionViewController.h"
#import "CPFileCell.h"

@interface CPFileViewController : CPBaseCollectionViewController

@end
