//
//  CPPickerCell.h
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseCell.h"

@interface CPTextLabelCell : CPBaseCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *content;

@end
