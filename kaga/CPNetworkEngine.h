//
//  CPNetworkEngine.h
//  akagi
//
//  Created by 王澍宇 on 15/9/21.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SVProgressHUD.h>
#import <AFNetworking.h>

#import "CPBaseModel.h"
#import "CPMarcos.h"
#import "CPCoding.h"
#import "CPAlert.h"

#define kTokenKey        @"TOKEN"
#define KDeviceToken     @"DEVICE_TOKEN_KEY"
#define kManagerStateKey @"USER_STATE_KEY"

static NSString *_appKey = @"appKey";

typedef enum : NSInteger {
    CPStatusError = -1,
    CPStatusSuccess,
} CPStatus;

typedef void(^CPRequestCallback)(NSDictionary *reseponseDic, NSError *error);
typedef void(^CPFetchRequestCallback)(NSArray *objects, NSError *error);

@interface CPNetworkEngine : NSObject

@property (nonatomic, strong) NSString *token;

@property (nonatomic, strong) NSString *deviceToken;

+ (instancetype)sharedEngine;

- (void)addOperationWithAPI:(NSString *)api Method:(NSString *)method Parms:(NSDictionary *)parms Callback:(CPRequestCallback)callback;

- (void)fetchModelWithAPI:(NSString *)apiName Parms:(NSDictionary *)parms Callback:(CPFetchRequestCallback)callback;

@end
