//
//  CPNetworkEngine.m
//  akagi
//
//  Created by 王澍宇 on 15/9/21.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPNetworkEngine.h"

static CPNetworkEngine *_engine;

@implementation CPNetworkEngine {
    NSString *_hostName;
    NSString *_apiVersion;
    
    NSURL *_baseURL;
    
    NSDictionary *_modelMap;
    
    NSDictionary *_apiDic;
    
    AFHTTPRequestSerializer *_httpRequestSerializer;
    AFJSONRequestSerializer *_jsonRequestSerializer;
}

@synthesize token       = _token;
@synthesize deviceToken = _deviceToken;

+ (instancetype)sharedEngine {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [[self alloc] init];
    });
    
    return _engine;
}

- (instancetype)init {
    if (self = [super init]) {
        NSString *apiConfigPath = [[NSBundle mainBundle] pathForResource:@"api" ofType:@"plist"];
        
        NSDictionary *apiConfigDic = [NSDictionary dictionaryWithContentsOfFile:apiConfigPath];
        
        _apiDic = [apiConfigDic copy];
        
        if (!_apiDic) {
            NSAssert(NO, @"API配置读取失败");
        }
        
#ifdef DEBUG
        _hostName   = _apiDic[@"DEBUG_HOST"];
#else
        _hostName   = _apiDic[@"RELEASE_HOST"];
#endif
        _modelMap   = _apiDic[@"MODEL_MAP"];
        _apiVersion = _apiDic[@"API_VERSION"];
        
        _baseURL    = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@", _hostName, _apiVersion]];
        
        _httpRequestSerializer = [AFHTTPRequestSerializer serializer];
        _jsonRequestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}

- (BOOL)networkReachable {
    return [[AFNetworkReachabilityManager sharedManager] isReachable] || [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusUnknown;
}

- (void)addOperationWithAPI:(NSString *)api Method:(NSString *)method Parms:(NSDictionary *)parms Callback:(CPRequestCallback)callback {
    
    BOOL isReachable = [self networkReachable];
    
    if (!isReachable) {
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *innerParms = [parms mutableCopy];
    
    innerParms[@"token"] = self.token;
    
    NSString *urlString = [[_baseURL URLByAppendingPathComponent:api] absoluteString];
    
    WeakSelf;
    
    void (^successBlock)(AFHTTPRequestOperation * _Nonnull, id  _Nonnull) = ^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        StrongSelf;
        
        if (s_self) {
            
            NSDictionary *responseDic = [responseObject copy];
        
            CPStatus errorStatus = [responseDic[@"error"] integerValue];
            
            NSString *message = [responseDic[@"msg"] copy];
            
            NSError *error = nil;
            
            if (errorStatus == CPStatusError) {
                error = [NSError errorWithDomain:message code:errorStatus userInfo:nil];
            }
            
            if (callback) {
                callback(responseDic, error);
            }
        }
    };
    
    void (^failureBlock)(AFHTTPRequestOperation * _Nonnull, NSError * _Nonnull) = ^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        
        NSError *localError = [NSError errorWithDomain:error.userInfo[NSLocalizedDescriptionKey] code:operation.response.statusCode userInfo:error.userInfo];
        
        NSLog(@"API: '%@' error with status code %ld , domain : '%@'", api, (long)localError.code, localError.domain);
        
        if (operation.response.statusCode == 401) {
            [self setToken:nil];
            [self dismissModalController];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLoginNotification object:self];
        }
        
        if (callback) {
            callback(nil, localError);
        }
    };
    
    if ([method isEqualToString:@"GET"]) {
        [manager setRequestSerializer:_httpRequestSerializer];
        [manager GET:urlString parameters:[innerParms copy] success:successBlock failure:failureBlock];
    } else if ([method isEqualToString:@"POST"]) {
        [manager setRequestSerializer:_jsonRequestSerializer];
        [manager POST:urlString parameters:[innerParms copy] success:successBlock failure:failureBlock];
    } else {
        NSAssert(NO, @"Unknown method for api currently!");
    }
    
}

- (void)fetchModelWithAPI:(NSString *)apiName Parms:(NSDictionary *)parms Callback:(CPFetchRequestCallback)callback {
    
    WeakSelf;
    
    [[CPNetworkEngine sharedEngine] addOperationWithAPI:apiName Method:@"GET" Parms:parms Callback:^(NSDictionary *reseponseDic, NSError *error) {
        
        StrongSelf;
        
        if (s_self) {
            
            NSMutableArray *objects = [NSMutableArray array];
            
            if (!error) {
                Class modelClass = NSClassFromString(s_self->_modelMap[apiName]);
                
                if (!modelClass) {
                    NSAssert(NO, @"No class match api:%@", apiName);
                }
                
                NSArray *results = reseponseDic[@"results"];
                
                NSError *modelError = nil;

                if ([results isKindOfClass:[NSNull class]]) {
                    results = [NSArray new];
                }
                
                objects = [[MTLJSONAdapter modelsOfClass:modelClass fromJSONArray:results error:&modelError] mutableCopy];
                
#ifdef DEBUG
                if (modelError) {
                    NSLog(@"%@", modelError);
                }
#endif
            }
            
            if (callback) {
                callback(objects, error);
            }
            
        }
        
    }];
}

#pragma mark - Helper Method 

- (void)dismissModalController {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    UIViewController *presentingController = [window.rootViewController presentedViewController];
    
    if (presentingController && [presentingController isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)presentingController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Token Setter & Getter

- (NSString *)token {
    if (!_token) {
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
    }
    
    return _token;
}

- (void)setToken:(NSString *)token {
    _token = token;
    [[NSUserDefaults standardUserDefaults] setObject:_token forKey:kTokenKey];
}

- (NSString *)deviceToken {
    if (!_deviceToken) {
        _deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:KDeviceToken];
    }
    
    return _deviceToken;
}

- (void)setDeviceToken:(NSString *)deviceToken {
    _deviceToken = deviceToken;
    [[NSUserDefaults standardUserDefaults] setObject:_deviceToken forKey:KDeviceToken];
}

@end
