//
//  CPTask.m
//  akagi
//
//  Created by 王澍宇 on 15/11/26.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPTask.h"

@implementation CPTask

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"         : @"task_id",
             @"fileName"   : @"file_name",
             @"pages"      : @"pages",
             @"bothSide"   : @"bothside",
             @"colorful"   : @"colorful",
             @"copies"     : @"copies"};
}


@end
