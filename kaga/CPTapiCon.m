//
//  CPVerticalButton.m
//  akagi
//
//  Created by 王澍宇 on 15/11/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPTapiCon.h"
#import "UILabel+Helper.h"

@implementation CPTapiCon

+ (instancetype)iConWithTitle:(NSString *)title
                     FontSize:(CGFloat)fontSize
                        Image:(UIImage *)image
                SelectedImage:(UIImage *)selectedImage
                        Color:(UIColor *)color
                SelectedColor:(UIColor *)selectedColor {
    
    CPTapiCon *icon = [[CPTapiCon alloc] init];
    
    icon.imageView = [[UIImageView alloc] initWithImage:image highlightedImage:selectedImage];
    icon.imageView.userInteractionEnabled = YES;
    
    icon.titleLabel = [UILabel labelWithText:title Color:color FontSize:fontSize Alignment:NSTextAlignmentCenter];
    icon.titleLabel.userInteractionEnabled = YES;
    
    icon.color = color;
    icon.selectedColor = selectedColor;
    
    [icon addSubview:icon.imageView];
    [icon addSubview:icon.titleLabel];
    
    [icon.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.centerX.equalTo(@0);
        make.size.mas_equalTo(CGSizeMake(25, 27));
    }];
    
    [icon.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(icon.imageView.mas_bottom).offset(4);
        make.centerX.equalTo(icon.imageView);
    }];
    
    return icon;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    
    _titleLabel.textColor  = _selected ? _selectedColor : _color;
    _imageView.highlighted = _selected ? YES : NO;
}

@end
