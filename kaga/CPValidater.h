//
//  CPValidater.h
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CPValidater : NSObject

+ (BOOL)validateName:(NSString *)name;

+ (BOOL)validatePassword:(NSString *)password;

+ (BOOL)validateEmail:(NSString *)email;

+ (BOOL)validateMobile:(NSString *)mobile;

+ (BOOL)validateQQ:(NSString *)qq;

+ (BOOL)validateIdentityCard: (NSString *)identityCard;

+ (BOOL)validateGraduateYear:(NSString *)year;

@end
