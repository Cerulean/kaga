//
//  UIWindow+Presentation.m
//  akagi
//
//  Created by 王澍宇 on 15/10/29.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "UIWindow+Presentation.h"

#import "CPNavigationController.h"

@implementation UIWindow (Presentation)

+ (void)presentViewController:(UIViewController *)viewController Animated:(BOOL)animated Completion:(void (void))completion {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    UIViewController *presentingViewController = nil;
    
    if ([[window rootViewController] presentedViewController]) {
        presentingViewController = [[window rootViewController] presentedViewController];
    } else {
        presentingViewController = [window rootViewController];
    }
    
    CPNavigationController *navigationController = [[CPNavigationController alloc] initWithRootViewController:viewController];
    
    [presentingViewController presentViewController:navigationController animated:YES completion:nil];
}

@end
