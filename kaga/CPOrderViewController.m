//
//  CPOrderViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderViewController.h"

@interface CPOrderViewController ()

@end

@implementation CPOrderViewController

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"订单";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    WeakSelf;
    
    if (![CPWebService hasLogin]) {
        return;
    }
    
    [self.viewModel fetchModelWithType:CPFetchModeNormal Block:^(NSArray *objects, NSError *error) {
        StrongSelf;
        
        if (!error) {
            [s_self.tableView reloadData];
        }
    }];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    CPBaseViewModel *viewModel = [[CPBaseViewModel alloc] initWithAPI:@"orders/detail"];
    viewModel.prams[@"order_id"] = @([(CPBaseModel *)[self.viewModel objects][indexPath.row] ID]);
    
}

@end
