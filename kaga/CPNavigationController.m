//
//  CPNavigationController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/9.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPNavigationController.h"
#import "CPTabBarController.h"

@interface CPNavigationController ()

@end

@implementation CPNavigationController

- (void)pushViewController:(Class)viewControllerClass ViewModel:(CPBaseViewModel *)viewModel animated:(BOOL)animated {
    if ([viewControllerClass isSubclassOfClass:[CPBaseViewController class]]) {
        
        CPBaseViewController *viewController = [[viewControllerClass alloc] init];
        viewController.viewModel = viewModel;
        
        [self pushViewController:viewController animated:YES];
    } else {
        NSAssert(NO, @"Class given is not subclass of CPBaseViewController");
    }
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.tabController && self.viewControllers.count == 1) {
        [UIView animateWithDuration:0.2 animations:^{
            self.tabController.tabBar.alpha = 0.0;
        }];
    }
    
    [super pushViewController:viewController animated:YES];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    
    if (self.tabController && self.viewControllers.count == 2) {
        [UIView animateWithDuration:0.2 animations:^{
            self.tabController.tabBar.alpha = 1.0;
        }];
    }
    
    return [super popViewControllerAnimated:animated];
}



@end
