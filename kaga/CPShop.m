//
//  CPShop.m
//  akagi
//
//  Created by 王澍宇 on 15/11/7.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPShop.h"

@implementation CPShop

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"         : @"shop_id",
             @"name"       : @"shop_name",
             @"location"   : @"shop_location",
             @"tel"        : @"shop_tel"};
}

@end
