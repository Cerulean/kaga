//
//  CPCheckViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/11/2.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPCheckViewController.h"

@implementation CPCheckViewController {
    NSMutableArray *_results;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"选择配送宿舍楼";
        
        UIBarButtonItem *item = [CPBarButtonItem itemWithTitle:@"完成"
                                                         Color:ColorMainTextContent
                                                        Target:self
                                                        Action:@selector(itemAction:)];
        self.navigationItem.rightBarButtonItem = item;
        
        _results = [NSMutableArray new];
    }
    return self;
}

- (void)itemAction:(UIBarButtonItem *)sender {
    NSArray *results = [_results copy];
    
    if (!results.count) {
        [CPAlert alertWithTitle:@"错误" Message:@"至少选择一个配送宿舍楼"];
        return;
    }
    
    if (_block) {
        _block(results);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    CPBaseCell *cell = (CPBaseCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        [_results addObject:self.viewModel.objects[indexPath.row]];
        
    } else {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        [_results removeLastObject];
    }
}

@end
