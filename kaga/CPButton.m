//
//  CPButton.m
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPButton.h"
#import "CPColor.h"

@implementation CPButton {
    CPButtonActionBlock _actionBlock;
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType ActionBlock:(CPButtonActionBlock)actionBlock {
    CPButton *button = [CPButton buttonWithType:buttonType];
    
    [button setActionBlock:actionBlock];
    
    return button;
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType Title:(NSString *)title FontSize:(CGFloat)fontSize ActionBlock:(CPButtonActionBlock)actionBlock {
    CPButton *button = [CPButton buttonWithType:buttonType ActionBlock:actionBlock];
    
    [button setTitleColor:ColorPlacehodlerBold forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:fontSize]];
    [button setTitle:title forState:UIControlStateNormal];
    
    return button;
}

- (void)doAction {
    
    if (_actionBlock) {
        _actionBlock(self);
    }
}

- (void)setActionBlock:(CPButtonActionBlock)actionBlock {
    _actionBlock = actionBlock;
    [self removeTarget:self action:@selector(doAction) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(doAction) forControlEvents:UIControlEventTouchUpInside];
}

@end
