//
//  CPUserViewController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/20.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewController.h"
#import "CPTextLabelCell.h"

@interface CPUserViewController : CPBaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) CPTextLabelCell *infoCell;

@property (nonatomic, strong) CPTextLabelCell *passwordCell;

@property (nonatomic, strong) CPTextLabelCell *ordersCell;

@property (nonatomic, strong) CPTextLabelCell *complainCell;

@property (nonatomic, strong) CPTextLabelCell *resignCell;

@end
