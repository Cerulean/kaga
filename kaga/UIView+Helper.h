//
//  UIView+Helper.h
//  akagi
//
//  Created by 王澍宇 on 15/11/4.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UIView (Helper)

typedef void(^CPTappedAction)(UIView *view);

@property (nonatomic, assign) BOOL isAnimating;

@property (nonatomic, strong) CPTappedAction tapAction;

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

- (void)animationWithDuration:(NSTimeInterval)duration animations:(void (^)())animations;

@end
