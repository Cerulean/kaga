//
//  CPTask.h
//  akagi
//
//  Created by 王澍宇 on 15/11/26.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"

@interface CPTask : CPBaseModel

@property (nonatomic, strong) NSString *fileName;

@property (nonatomic, assign) NSInteger pages;

@property (nonatomic, assign) NSInteger copies;

@property (nonatomic, assign) BOOL bothSide;

@property (nonatomic, assign) BOOL colorful;

@end
