//
//  CPManager.h
//  akagi
//
//  Created by 王澍宇 on 15/10/17.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"
#import "CPSchool.h"

typedef enum : NSInteger {
    CPManagerStateUnauthorized = -1,
    CPManagerStateUnfinishInfo,
    CPManagerStateAuthorizing,
    CPManagerStateAuthorized,
    CPManagerStateResigned,
} CPManagerState;

typedef enum : NSInteger {
    CPManagerGenderUnknown = -1,
    CPManagerGenderMale,
    CPManagerGenderFemale,
} CPManagerGender;

@interface CPManager : CPBaseModel

@property (nonatomic, strong) NSString *username;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, assign) CPManagerState state;

@property (nonatomic, assign) CPManagerGender sex;

@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSString *qq;

@property (nonatomic, strong) NSString *idCardNumber;

@property (nonatomic, strong) NSString *alipayAccount;

@property (nonatomic, strong) NSString *graduateYear;

@property (nonatomic, strong) CPSchool *school;

@property (nonatomic, strong) CPBuilding *building;

@property (nonatomic, strong) NSArray<CPBuilding *> *manageBuildings;

@property (nonatomic, strong) NSString *dorm;

@property (nonatomic, strong) NSString *studentCard;

@end
