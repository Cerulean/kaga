//
//  CPBarButtonItem.h
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPBarButtonItem : UIBarButtonItem

+ (UIBarButtonItem *)itemWithTitle:(NSString *)title Color:(UIColor *)color Target:(id)target Action:(SEL)action;

@end
