//
//  CPBaseCollectionViewController.h
//  kaga
//
//  Created by 王澍宇 on 15/12/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <MJRefresh.h>

#import "CPBaseViewController.h"

@interface CPBaseCollectionViewController : CPBaseViewController <UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UICollectionViewLayout *layout;

@property (nonatomic, assign) BOOL enableUpRefresh;

@property (nonatomic, assign) BOOL enableDownRrefresh;

@end
