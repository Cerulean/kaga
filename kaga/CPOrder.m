//
//  CPOrder.m
//  akagi
//
//  Created by 王澍宇 on 15/10/18.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrder.h"

@implementation CPOrder

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"         : @"ID",
             @"tradeID"    : @"trade_no",
             @"state"      : @"order_state",
             @"type"       : @"order_type",
             @"dorm"       : @"student_room",
             @"tel"        : @"student_phone",
             @"nickName"   : @"nickname",
             @"paidDate"   : @"paid_at",
             @"demandDate" : @"demand_time",
             @"shop"       : @"shop"};
}

+ (NSValueTransformer *)paidDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSNumber *timestamp, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)shopJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[CPShop class]];
}


@end
