//
//  CPComplaints.m
//  akagi
//
//  Created by 王澍宇 on 15/11/6.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPComplaints.h"

@implementation CPComplaints

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"comment" : @"complain",
             @"date"    : @"complain_at",
             @"ID"      : @"ID"};
}

+ (NSValueTransformer *)commentAtJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString * dateString, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [self.dateFormatter dateFromString:dateString];
        return [self.dateFormatter stringFromDate:date];
    }];
}

@end
