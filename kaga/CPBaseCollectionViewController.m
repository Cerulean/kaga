//
//  CPBaseCollectionViewController.m
//  kaga
//
//  Created by 王澍宇 on 15/12/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseCollectionViewController.h"
#import "CPRefreshHeader.h"

@interface CPBaseCollectionViewController ()

@end

@implementation CPBaseCollectionViewController

- (instancetype)init {
    if (self = [super init]) {
        _enableUpRefresh    = YES;
        _enableDownRrefresh = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:_layout ? : [UICollectionViewFlowLayout new]];
    _collectionView.dataSource      = self.viewModel;
    _collectionView.delegate        = self;
    _collectionView.contentInset    = UIEdgeInsetsMake(0, 0, STATUS_HEIGHT + TABBAR_HEIGHT + COMMON_HEIGHT, 0);
    _collectionView.backgroundColor = ColorWhite;
    
    [self.view addSubview:_collectionView];
    
    WeakSelf;
    
    if (_enableUpRefresh) {
        _collectionView.mj_header = [CPRefreshHeader headerWithRefreshingBlock:^{
            StrongSelf;
            
            [s_self.viewModel fetchModelWithType:CPFetchModeNew Block:^(NSArray *objects, NSError *error) {
                if (!error) {
                    [s_self.collectionView reloadData];
                }
                [s_self.collectionView.mj_header endRefreshing];
            }];
        }];
    }
    
    if (_enableDownRrefresh) {
        _collectionView.mj_footer = [MJRefreshBackFooter footerWithRefreshingBlock:^{
            StrongSelf;
            
            [s_self.viewModel fetchModelWithType:CPFetchModeMore Block:^(NSArray *objects, NSError *error) {
                if (!error) {
                    [s_self.collectionView reloadData];
                }
                [s_self.collectionView.mj_footer endRefreshing];
            }];
            
        }];
    }
    
}

@end
