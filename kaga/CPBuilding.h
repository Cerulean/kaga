//
//  CPBuilding.h
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"

typedef enum : NSUInteger {
    CPBuildingStateUnopened,
    CPBuildingStateOpened,
} CPBuildingState;

@interface CPBuilding : CPBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, assign) CPBuildingState status;

@end
