//
//  CPTabBarController.m
//  akagi
//
//  Created by 王澍宇 on 15/11/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPTabBarController.h"

@implementation CPTabBarController {
    UIView *_tabLine;
    
    NSMutableArray *_icons;
}

- (instancetype)initWithViewControllers:(NSArray *)viewControllers Titles:(NSArray *)titles Images:(NSArray *)images SelectedImages:(NSArray *)selectedImages {
    if (self = [super init]) {
        
        _viewControllers = [viewControllers copy];
        _titles          = [titles copy];
        _images          = [images copy];
        _selectedImages  = [selectedImages copy];
        
        _icons = [NSMutableArray new];
        
        
        _tabBar = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - TABBAR_HEIGHT, SCREEN_WIDTH, TABBAR_HEIGHT)];
        _tabBar.backgroundColor = ColorWhite;
        
        [self.view addSubview:_tabBar];
        
        _tabLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
        _tabLine.backgroundColor = ColorPlacehodler;
        
        [_tabBar addSubview:_tabLine];
        
        WeakSelf;
        
        CGFloat iconWidth = SCREEN_WIDTH / _viewControllers.count;
        
        for (NSInteger index = 0; index < _viewControllers.count; ++index) {
            
            CPTapiCon *icon = [CPTapiCon iConWithTitle:_titles[index]
                                              FontSize:10
                                                 Image:_images[index]
                                         SelectedImage:_selectedImages[index]
                                                 Color:ColorSubTextContent
                                         SelectedColor:ColorMainTextContent];
            
            icon.tapAction = ^(UIView *view) {
                
                StrongSelf;
                
                CPTapiCon *aIcon = (CPTapiCon *)view;
                
                [aIcon setSelected:YES];
                
                for (CPTapiCon *__icon in s_self->_icons) {
                    if (__icon != aIcon) {
                        [__icon setSelected:NO];
                    }
                }
                
                [s_self loadViewController:s_self.viewControllers[index]];
            };
            
            [_icons addObject:icon];
            
            [_tabBar addSubview:icon];
            
            [icon mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(_tabBar).offset(index * iconWidth);
                make.top.equalTo(@8);
                make.bottom.equalTo(@0);
                make.width.equalTo(@(iconWidth));
            }];
            
            CPNavigationController *naviController = _viewControllers[index];
            naviController.tabController = self;
        }
        
        [[_icons firstObject] setSelected:YES];

        [self loadViewController:[_viewControllers firstObject]];
    }
    return self;
}

- (void)loadViewController:(CPNavigationController *)naviController {
    
    if (_currentController == naviController) {
        return;
    }
    
    [self.currentController.view removeFromSuperview];
    
    [self setCurrentController:naviController];
    
    [self.view addSubview:_currentController.view];
    
    [self addChildViewController:_currentController];
    
    [self.view bringSubviewToFront:_tabBar];
}

@end
