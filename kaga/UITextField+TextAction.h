//
//  UITextField+TextAction.h
//  akagi
//
//  Created by 王澍宇 on 15/11/3.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

typedef void(^CPTextAction)(NSString *text);

@interface UITextField (TextAction)

@property (nonatomic, strong) CPTextAction textAction;

@property (nonatomic, strong) CPTextAction beginAction;

@property (nonatomic, strong) CPTextAction doneAction;

@end
