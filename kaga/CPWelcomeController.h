//
//  CPMainViewController.h
//  akagi
//
//  Created by 王澍宇 on 15/11/4.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewController.h"

typedef enum : NSUInteger {
    CPWelcomeActionTypeLogin,
    CPWelcomeActionTypeSignup,
    CPWelcomeActionTypeForget,
} CPWelcomeActionType;

@interface CPWelcomeController : CPBaseViewController

@end
