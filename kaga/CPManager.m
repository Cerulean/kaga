//
//  CPManager.m
//  akagi
//
//  Created by 王澍宇 on 15/10/17.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPManager.h"

@implementation CPManager

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"username"        : @"username",
             @"name"            : @"manager_name",
             @"sex"             : @"sex",
             @"email"           : @"email",
             @"qq"              : @"qq",
             @"idCardNumber"    : @"ID_number",
             @"alipayAccount"   : @"alipay_account",
             @"graduateYear"    : @"graduate_year",
             @"school"          : @"school",
             @"building"        : @"building",
             @"manageBuildings" : @"manage_buildings",
             @"dorm"            : @"dorm",
             @"studentCard"     : @"certificate"};
}

+ (NSValueTransformer *)schoolJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[CPSchool class]];
}

+ (NSValueTransformer *)buildingJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[CPBuilding class]];
}

+ (NSValueTransformer *)manageBuildingsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[CPBuilding class]];
}

- (instancetype)init {
    if (self = [super init]) {
        self.sex      = CPManagerGenderUnknown;
    }
    return self;
}

@end
