//
//  CPOrderDetail.m
//  akagi
//
//  Created by 王澍宇 on 15/11/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderDetail.h"

@implementation CPOrderDetail

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"fee"        : @"fee",
             @"deliverFee" : @"delivery_fee",
             @"rate"       : @"rate",
             @"note"       : @"note",
             @"tasks"      : @"tasks",
             @"comment"    : @"comment",};
}

+ (NSValueTransformer *)tasksJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[CPTask class]];
}

@end
