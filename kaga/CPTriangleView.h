//
//  CPTriangleView.h
//  akagi
//
//  Created by 王澍宇 on 15/11/4.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPTriangleView : UIView

@property (nonatomic, strong) CAShapeLayer *triangleLayer;

@end
