//
//  CPBaseViewModel.m
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewModel.h"

@implementation CPBaseViewModel {
    BOOL _isFetching;
}

- (instancetype)initWithAPI:(NSString *)api {
    if (self = [super init]) {
        _api     = api;
        _prams   = [NSMutableDictionary dictionary];
        _objects = [NSMutableArray array];
    }
    return self;
}

- (instancetype)initWithAPI:(NSString *)api CellClass:(Class)cellClass {
    if (self = [self initWithAPI:api]) {
        _cellClass = cellClass;
    }
    return self;
}

- (void)fetchModelWithType:(CPFetchMode)mode Block:(CPFetchModelBlock)block {
    
    if (_isFetching) {
        return;
    }
    
    _isFetching = YES;
    
    NSMutableDictionary *parms = [[NSMutableDictionary alloc] initWithDictionary:_prams];
    
    if (mode == CPFetchModeNew) {
        parms[@"firstID"] = @([(CPBaseModel *)[_objects firstObject] ID]);
    } else if (mode == CPFetchModeMore) {
        parms[@"lastID"]  = @([(CPBaseModel *)[_objects lastObject] ID]);
    } else {
        // do nothing
    }
    
    WeakSelf;
    
    [[CPNetworkEngine sharedEngine] fetchModelWithAPI:_api Parms:[parms copy] Callback:^(NSArray *objects, NSError *error) {
        
        StrongSelf;
        
        if (!error) {
            
            switch (mode) {
                case CPFetchModeNew:
                    
                    [s_self.objects insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, objects.count)]];
                    
                    break;
                    
                case CPFetchModeNormal:
                case CPFetchModeMore:
                    
                    [s_self.objects addObjectsFromArray:objects];
                    
                    break;
                
                case CPFetchModeClear:
                    
                    [s_self.objects removeAllObjects];
                    [s_self.objects addObjectsFromArray:objects];
                    
                    break;
                    
                default:
                    break;
            }
            
            if (block) {
                block(objects, nil);
            }
            
        } else {
            if (block) {
                block(nil, error);
            }
        }
        
        s_self->_isFetching = NO;
    }];
}

- (void)removeObject:(CPBaseModel *)object Block:(void (^)())block {
    CPBaseModel *flagModel = nil;
    
    flagModel = [self objectWithID:object.ID];
    
    if (flagModel) {
        [_objects removeObject:flagModel];
        
        if (block) {
            block();
        }
    }
}

#pragma mark - Private Method 

- (CPBaseModel *)objectWithID:(NSInteger)ID {
    for (CPBaseModel *model in _objects) {
        if (model.ID ==ID) {
            return model;
        }
    }
    return nil;
}

#pragma mark - UITableViewDataSource and UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reuseIdentifier = @"CellReuseIdentifier";
    
    CPBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[_cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        cell.tableView = tableView;
        cell.viewModel = self;
    }
    
    [cell bindWithModel:_objects[indexPath.row]];
    
    return cell;
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _objects.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reuseIdentifer = @"ItemReuseIdentifier";
    
    CPBaseCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifer forIndexPath:indexPath];
    
    [cell bindWithModel:_objects[indexPath.row]];
    
    return cell;
}

@end
