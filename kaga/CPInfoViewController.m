//
//  CPUpdateInfoController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/21.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPInfoViewController.h"

@interface CPInfoViewController ()

@end

@implementation CPInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *leftItem = [CPBarButtonItem itemWithTitle:@"返回" Color:ColorMainTextContent Target:self Action:@selector((goBack))];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    self.cardLabel.userInteractionEnabled = NO;
    self.cardLabel.content = @"已上传";
    
    WeakSelf;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [self.viewModel fetchModelWithType:CPFetchModeNormal Block:^(NSArray *objects, NSError *error) {
        
        StrongSelf;
        
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.domain];
            return;
        } else {
            [SVProgressHUD showSuccessWithStatus:@"获取个人信息成功"];
        }
        
        s_self.manager = [objects firstObject];
        
        s_self.nameLabel.textField.text     = s_self.manager.name;
        s_self.sexLabel.content             = s_self.manager.sex ? @"女" : @"男";
        s_self.emailLabel.textField.text    = s_self.manager.email;
        s_self.qqLabel.textField.text       = s_self.manager.qq;
        s_self.idCardLabel.textField.text   = s_self.manager.idCardNumber;
        s_self.alipayLabel.textField.text   = s_self.manager.alipayAccount;
        s_self.graduateLabel.textField.text = s_self.manager.graduateYear;
        s_self.schoolLabel.content          = s_self.manager.school.name;
        s_self.buildingLabel.content        = s_self.manager.building.name;
        s_self.manageBuildingsLabel.content = s_self.manager.manageBuildings.count ? @"已选择" : @"未选择";
        s_self.dormLabel.textField.text     = s_self.manager.dorm;
    }];
}

- (void)finishModify:(UIBarButtonItem *)sender {
    
    if (![CPValidater validateName:self.manager.name]) {
        [CPAlert alertWithTitle:@"错误" Message:@"中文姓名长度不合法"];
        return;
    }
    
    if (![CPValidater validateEmail:self.manager.email]) {
        [CPAlert alertWithTitle:@"错误" Message:@"邮箱格式不合法"];
        return;
    }
    
    if (![CPValidater validateQQ:self.manager.qq]) {
        [CPAlert alertWithTitle:@"错误" Message:@"QQ号码长度不合法"];
        return;
    }
    
    if (![CPValidater validateIdentityCard:self.manager.idCardNumber]) {
        [CPAlert alertWithTitle:@"错误" Message:@"身份证长度不合法"];
        return;
    }
    
    if (![CPValidater validateEmail:self.manager.alipayAccount] && ![CPValidater validateMobile:self.manager.alipayAccount]) {
        [CPAlert alertWithTitle:@"错误" Message:@"支付宝账号格式不合法"];
        return;
    }
    
    if (![CPValidater validateGraduateYear:self.manager.graduateYear]) {
        [CPAlert alertWithTitle:@"错误" Message:@"毕业年份不合法"];
        return;
    }
    
    if (self.manager.sex == CPManagerGenderUnknown) {
        [CPAlert alertWithTitle:@"错误" Message:@"性别类型不能为空"];
        return;
    }
    
    if (!self.manager.school) {
        [CPAlert alertWithTitle:@"错误" Message:@"学校不能为空"];
        return;
    }
    
    if (!self.manager.building) {
        [CPAlert alertWithTitle:@"错误" Message:@"所在宿舍楼不能为空"];
        return;
    }
    
    if (!self.manager.manageBuildings) {
        [CPAlert alertWithTitle:@"错误" Message:@"未选择配送宿舍楼"];
        return;
    }
    
    NSDictionary *parms = nil;
    
    parms = [MTLJSONAdapter JSONDictionaryFromModel:self.manager error:NULL];
    
    WeakSelf;
    
    [CPWebService commonRequestWithAPI:@"manager/info/update" Method:@"POST" Parms:[parms copy] Block:^(BOOL success, NSDictionary *resultDic) {
        if (success) {
            StrongSelf;
            [s_self.navigationController popViewControllerAnimated:YES];
        }
    }];

}

@end
