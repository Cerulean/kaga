//
//  CPOrderCell.h
//  akagi
//
//  Created by 王澍宇 on 15/10/19.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseCell.h"

#import "CPOrder.h"

@interface CPOrderCell : CPBaseCell

@property (nonatomic, strong) UILabel *typeLabel;

@property (nonatomic, strong) UIImageView *typeIcon;

@property (nonatomic, strong) UILabel *locationLabel;

@property (nonatomic, strong) UIImageView *locationIcon;

@property (nonatomic, strong) UILabel *nickNameLabel;

@property (nonatomic, strong) UILabel *telLabel;

@property (nonatomic, strong) UILabel *dormLabel;

@property (nonatomic, strong) UILabel *paidDateLabel;

@property (nonatomic, strong) UIImageView *paidIcon;

@property (nonatomic, strong) UILabel *demandDateLabel;

@property (nonatomic, strong) UIImageView *demandIcon;

@property (nonatomic, strong) CPButton *handleButton;

@property (nonatomic, strong) UIImageView *arrow;

@property (nonatomic, strong) UIView *infoView;

@property (nonatomic, strong) UIView *topMarginView;

@property (nonatomic, strong) UIView *downMarginView;

@end
