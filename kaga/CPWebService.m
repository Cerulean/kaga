//
//  CPWebService.m
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPWebService.h"

#import "CPWelcomeController.h"
#import "CPModifyInfoController.h"
#import "CPWelcomeController.h"

static BOOL _isDoingLogin = NO;

@implementation CPWebService

+ (void)initialize {
    [[NSNotificationCenter defaultCenter] addObserverForName:kNeedLoginNotification
                                                      object:[CPNetworkEngine sharedEngine]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                                                                   (int64_t)(1 * NSEC_PER_SEC)),
                                                                     dispatch_get_main_queue(), ^{
                                                                         
                                                          [CPWebService checkIfNeedLoginWithBlock:nil];
                                                                         
                                                      });
    }];
}

+ (BOOL)hasLogin {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey] ? YES : NO;
}

+ (BOOL)needFinishInfo {
    return ![[[NSUserDefaults standardUserDefaults] objectForKey:kManagerStateKey] integerValue];
}

+ (void)logOut {
    [CPWebService commonRequestWithAPI:@"manager/logout" Method:@"POST" Parms:[NSDictionary new] Block:^(BOOL success, NSDictionary *resultDic) {
        if (success) {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTokenKey];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDeviceToken];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kManagerStateKey];
            
            [CPWebService checkIfNeedLoginWithBlock:nil];
        }
    }];
}

+ (void)checkIfNeedFinishInfoWithBlock:(void (^)(BOOL))block {
    BOOL need = [CPWebService needFinishInfo];

    if (block) {
        block(need);
    }
    
    if (!need) {
        return;
    }
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    UIViewController *presentingViewController = [[window rootViewController] presentedViewController];
    
    BOOL isModal = presentingViewController ? YES : NO;
    
    if (!isModal) {
        presentingViewController = [window rootViewController];
    }
    
    if (isModal && [presentingViewController isKindOfClass:[CPNavigationController class]]) {
        [(CPNavigationController *)presentingViewController pushViewController:[CPModifyInfoController new] animated:YES];
    } else {
        CPModifyInfoController *modifyInfoController = [CPModifyInfoController new];
        modifyInfoController.navigationItem.leftBarButtonItem = nil;
        
        CPNavigationController *modifyNaviController = [[CPNavigationController alloc] initWithRootViewController:modifyInfoController];

        
        [presentingViewController presentViewController:modifyNaviController animated:YES completion:nil];
    }
}

+ (void)checkIfNeedLoginWithBlock:(void (^)(BOOL))block {
    
    if (_isDoingLogin) {
        return;
    } else {
        _isDoingLogin = YES;
    }
    
    BOOL need = ![CPWebService hasLogin];
    
    if (block) {
        block(need);
    }
    
    _isDoingLogin = NO;
    
    if (!need) {
        return;
    }
    
    [UIWindow presentViewController:[CPWelcomeController new] Animated:YES Completion:nil];
}


+ (void)loginWithParms:(NSDictionary *)parms Block:(void (^)(BOOL))block {
    
    NSMutableDictionary *localParms = [parms mutableCopy];
    localParms[@"device_token"] = [[CPNetworkEngine sharedEngine] deviceToken];
    
    [CPWebService commonRequestWithAPI:@"manager/login" Method:@"POST" Parms:[localParms copy] Block:^(BOOL success, NSDictionary *resultDic) {
        if (success) {
            
            NSString *token = resultDic[@"token"];
            
            [[CPNetworkEngine sharedEngine] setToken:token];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if (block) {
            block(success);
        }
    }];
}

+ (void)signUpWithParms:(NSDictionary *)parms Block:(void (^)(BOOL))block {
    
    [CPWebService commonRequestWithAPI:@"manager/signup" Method:@"POST" Parms:[parms copy] Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (success) {
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *token = resultDic[@"token"];
            
            [[CPNetworkEngine sharedEngine] setToken:token];
        }
        
        if (block) {
            block(success);
        }
        
    }];
}

+ (void)resignWithBlock:(void (^)(BOOL success))block {
    [CPWebService commonRequestWithAPI:@"manager/resign" Method:@"POST" Parms:[NSDictionary new] Block:^(BOOL success, NSDictionary *resultDic) {
        if (success) {
            [[CPNetworkEngine sharedEngine] setToken:nil];
            [CPWebService checkIfNeedLoginWithBlock:nil];
        }
    }];
}

+ (void)commonRequestWithAPI:(NSString *)api Method:(NSString *)method Parms:(NSDictionary *)parms Block:(void (^)(BOOL, NSDictionary *))block {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CPNetworkEngine sharedEngine] addOperationWithAPI:api Method:method Parms:[parms copy] Callback:^(NSDictionary *reseponseDic, NSError *error) {
        
        BOOL isSuccess = error ? NO : YES;
        
        NSString *message = reseponseDic[@"msg"];
        
        if (isSuccess) {
            [SVProgressHUD showSuccessWithStatus:message];
        } else {
            [SVProgressHUD showErrorWithStatus:message];
        }
        
        NSDictionary *resultDic = [reseponseDic[@"results"] firstObject];
        
        if (block) {
            block(isSuccess, resultDic);
        }
    }];
}

@end
