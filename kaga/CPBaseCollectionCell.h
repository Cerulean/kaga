//
//  CPBaseCollectionCell.h
//  kaga
//
//  Created by 王澍宇 on 15/12/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPCommonTools.h"

@interface CPBaseCollectionCell : UICollectionViewCell

@property (nonatomic, weak) CPBaseViewModel *viewModel;

@property (nonatomic, strong) CPBaseModel *model;

- (void)bindWithModel:(CPBaseModel *)model;

@end
