//
//  CPAlert.m
//  akagi
//
//  Created by 王澍宇 on 15/10/9.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPAlert.h"

@implementation CPAlert

+ (void)alertWithErrorCode:(NSInteger)errorCode errorMessage:(NSString *)errorMessage {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"错误码:%ld", (long)errorCode]
                                                                             message:errorMessage
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:okAction];
    
    [CPAlert showAlertController:alertController];
}

+ (void)alertWithTitle:(NSString *)title Message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"知道了"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];

    
    [alertController addAction:confirmAction];
    
    [CPAlert showAlertController:alertController];
}

+ (void)alertWithTitle:(NSString *)title Message:(NSString *)message Stressed:(BOOL)stressed confirmAction:(void (^)())action {
    UIAlertController *alertController = [CPAlert alertControllerWithTitle:title Message:message PreferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确认"
                                                            style:stressed ? UIAlertActionStyleDestructive : UIAlertActionStyleDefault
                                                          handler:action];
    
    UIAlertAction *cancelAction  = [UIAlertAction actionWithTitle:@"取消"
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil];
    
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    
    [CPAlert showAlertController:alertController];
}

+ (void)actionSheetWithTitle:(NSString *)title Message:(NSString *)message Stressed:(BOOL)stressed confirmAction:(void (^)())action {
    UIAlertController *alertController = [CPAlert alertControllerWithTitle:title Message:message PreferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确认"
                                                            style:stressed ? UIAlertActionStyleDestructive : UIAlertActionStyleDefault
                                                          handler:action];
    
    UIAlertAction *cancelAction  = [UIAlertAction actionWithTitle:@"取消"
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil];
    
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    
    [CPAlert showAlertController:alertController];
}

+ (UIAlertController *)alertControllerWithTitle:(NSString *)title Message:(NSString *)message  PreferredStyle:(UIAlertControllerStyle)preferredStyle {
    return [UIAlertController alertControllerWithTitle:title message:message preferredStyle:preferredStyle];
    
}

+ (void)showAlertController:(UIAlertController *)controller {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIViewController *presentingViewController = nil;
    
    if ([[keyWindow rootViewController] presentedViewController]) {
        presentingViewController = [[keyWindow rootViewController] presentedViewController];
    } else {
        presentingViewController = [keyWindow rootViewController];
    }
    
    [presentingViewController presentViewController:controller animated:YES completion:nil];
}

@end
