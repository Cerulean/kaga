//
//  CPColor.h
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <EDColor.h>

#define ColorMainTextContent [CPColor colorWithHex:0x394251]
#define ColorSubTextContent  [CPColor colorWithHex:0x595959]
#define ColorLine            [CPColor colorWithHex:0xE4EAF4]
#define ColorLightWhile      [CPColor colorWithHex:0xFFFFFF andAlpha:0.75]
#define ColorWhite           [CPColor colorWithHex:0xFFFFFF]
#define ColorMainBackground  [CPColor colorWithHex:0x66CCFF]
#define ColorSubBackground   [CPColor colorWithHex:0xFAFAFA]
#define ColorPlacehodler     [CPColor colorWithHex:0xD2D2DA]
#define ColorPlacehodlerBold [CPColor colorWithHex:0xCFCFCF]
#define ColorSection         [CPColor colorWithHex:0xF8F8F8]
#define ColorWarning         [CPColor colorWithHex:0xFF0000]

@interface CPColor : NSObject

+ (UIColor *)colorWithHex:(UInt32)hex;

+ (UIColor *)colorWithHex:(UInt32)hex andAlpha:(CGFloat)alpha;

@end
